# NestJS JWT REST Auth Starter App
## Installation

```bash
$ pnpm install
```

Create enviromental .env file `.env.development` or `.env.production` and fill it with configuration commented in `.env` file
## Running the app

```bash
# development
$ pnpm run start

# watch mode
$ pnpm run start:dev

# production mode
$ pnpm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
