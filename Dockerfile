# Base image
FROM node:20.2.0-alpine

# Create app directory
WORKDIR /usr/src/app

# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

# Install pnpm globally
RUN npm install -g pnpm

# Install app dependencies
RUN pnpm install

# Bundle app source
COPY . .

# Creates a "dist" folder with the production build
#RUN pnpm run build

# Start the server using the production build
#CMD [ "node", "dist/main.js" ]
CMD [ "pnpm", "start:dev" ]

