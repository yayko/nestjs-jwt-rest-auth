import { Column, Entity, Index, OneToMany, Unique } from 'typeorm';
import { RbacPermission, UserRole } from '@/common/entities'

@Unique('rbac_role_pkey', ['name'])
@Index('rbac_role_name_idx', ['name'])
@Entity()
export class RbacRole {
  @Column({
    primary: true,
    length: 64,
    primaryKeyConstraintName: 'rbac_role_pkey',
  })
  name: string;

  @Column({ type: 'text', nullable: true })
  description: string | null;

  @Column({
    type: 'timestamp without time zone',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date | null;

  @Column({
    type: 'timestamp without time zone',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date | null;

  @OneToMany(
    () => RbacPermission,
    (rbacPermission) => rbacPermission.rbacRole,
  )
  rbacPermissions: RbacPermission[];

  @OneToMany(() => UserRole, (userRole) => userRole.roleName)
  userRoles: UserRole[];
}
