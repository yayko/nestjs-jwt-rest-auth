import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm'
import { User } from '@/common/entities'

const PKEY_NAME = 'user_role_pkey'

@Index('user_role_userId_idx', ['userId'])
@Entity('user_role')
export class UserRole {
  @PrimaryColumn({ length: 64, primaryKeyConstraintName: PKEY_NAME })
  roleName: string

  @PrimaryColumn({  primaryKeyConstraintName: PKEY_NAME })
  userId: string

  @Column({
    type: 'timestamp without time zone',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP'
  })
  createdAt: Date | null

  @ManyToOne(() => User, user => user.userRoles, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User
}
