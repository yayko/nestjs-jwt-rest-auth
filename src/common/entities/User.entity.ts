import { Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { UserRole } from '@/common/entities'
import { TimestampColumn } from '@/common/decorators'

@Index('user_id_email_idx', ['email', 'id'])
@Index('user_id_status_idx', ['status', 'id'])
@Index('user_email_uidx', ['email'], { unique: true })
@Index('user_password_reset_token_uidx', ['passwordResetToken'], {
  unique: true
})
@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid', { primaryKeyConstraintName: 'user_pkey' })
  id: string

  @Column()
  email: string

  @Column({
    nullable: true
  })
  firstName: string | null

  @Column({
    nullable: true
  })
  lastName: string | null

  @Column('smallint', { default: () => '10' })
  status: number

  @Column()
  passwordHash: string

  @Column({
    nullable: true
  })
  passwordResetToken: string | null

  @Column({
    nullable: true
  })
  refreshTokenHash: string | null

  @Column({
    nullable: true
  })
  verificationToken: string | null

  @TimestampColumn({
    default: undefined
  })
  verifiedAt: Date | null

  @TimestampColumn()
  createdAt: Date | null

  @TimestampColumn()
  updatedAt: Date | null

  @OneToMany(() => UserRole, userRole => userRole.user)
  userRoles: UserRole[]
}
