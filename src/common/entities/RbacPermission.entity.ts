import { RbacRole } from '@/common/entities'
import { Column, Entity, Index, ManyToOne, PrimaryColumn } from 'typeorm'

const PKEY_NAME = 'rbac_permission_pkey'

@Index('rbac_permission_name_idx', ['name'], {unique: true})
@Entity()
export class RbacPermission {
  @PrimaryColumn({
    length: 64,
    primaryKeyConstraintName: PKEY_NAME
  })
  name: string

  @Column({ type: 'text', nullable: true })
  description: string | null


  @Column({
    type: 'timestamp without time zone',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP'
  })
  createdAt: Date | null

  @Column({
    type: 'timestamp without time zone',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP'
  })
  updatedAt: Date | null

  @ManyToOne(() => RbacRole, rbacRole => rbacRole.rbacPermissions, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE'
  })
  rbacRole: RbacRole
}
