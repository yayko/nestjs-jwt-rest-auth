import { Column, ColumnOptions } from 'typeorm'

type TimestampColumnOptions = Omit<ColumnOptions, 'type'>

export function TimestampColumn(options?: TimestampColumnOptions): PropertyDecorator {
  return Column({
    type: 'timestamp without time zone',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
    ...(options ?? {})
  })
}
