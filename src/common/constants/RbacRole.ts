export const RbacRole = {
  ROOT: 'root',
  ADMIN: 'admin',
  USER: 'user',
};
