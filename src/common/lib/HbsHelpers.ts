import Handlebars from 'handlebars';

export const registerHbsHelpers = () => {
  Handlebars.registerHelper('add', function (a: number, b: number) {
    return a + b;
  });

  Handlebars.registerHelper('multiply', function (a: number, b: number) {
    return a * b;
  });

  Handlebars.registerHelper('formatDate', function (date: string) {
    return new Date(date).toLocaleDateString('pl-PL');
  });

  Handlebars.registerHelper('formatCurrency', function (value: number) {
    return value.toFixed(2).replace('.', ',') + ' PLN';
  });
  Handlebars.registerHelper('isDefined', function (value: string) {
    return (
      value !== undefined &&
      value != 'undefined' &&
      value.length > 0 &&
      value !== ''
    );
  });
  Handlebars.registerHelper('notEmptyObject', function (value) {
    return value !== null && Object.keys(value).length;
  });
};
