import { JwtTokens } from '@/types';
import { serialize } from 'cookie';
import { Response } from 'express';

export default class AuthCookie {
  static setHeader(res: Response, { accessToken, refreshToken }: JwtTokens) {
    res.setHeader(
      'set-cookie',
      AuthCookie.getSerializedArray({ accessToken, refreshToken }),
    );
  }

  static removeHeader(res: Response) {
    res.setHeader('set-cookie', [
      serialize(`accessToken`, '', {
        expires: new Date(Date.now() - 1),
        httpOnly: true,
        sameSite: true,
        path: '/',
      }),
      serialize(`refreshToken`, '', {
        expires: new Date(Date.now() - 1),
        httpOnly: true,
        sameSite: true,
        path: '/',
      }),
    ]);
  }

  static getSerializedArray({ accessToken, refreshToken }: JwtTokens) {
    const authTokenCookieMaxAge = 60 * 60 * 1 * 1000;
    const refreshTokenCookieMaxAge = 60 * 60 * 24 * 7 * 1000;
    return [
      serialize(`accessToken`, accessToken, {
        expires: new Date(Date.now() + authTokenCookieMaxAge),
        httpOnly: true,
        secure: process.env.NODE_ENV === 'production',
        path: '/',
        // sameSite: "lax",
      }),
      serialize(`refreshToken`, refreshToken, {
        expires: new Date(Date.now() + refreshTokenCookieMaxAge),
        httpOnly: true,
        secure: process.env.NODE_ENV === 'production',
        path: '/',
        // sameSite: "lax",
      }),
    ];
  }
}
