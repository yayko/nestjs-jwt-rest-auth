const stringToShuffle =
  'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPrRqQsStTuUvVwWxXyYzZ11223344556677889900';
export const shuffleString = (str: string, times = 10) => {
  const a = str.split('');

  for (let i = times; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
  }

  return a.join('');
};

export const randomString = (length = 32) => {
  if (length > 64) throw new Error('Max. 64 characters.');
  return shuffleString(stringToShuffle).substring(0, length - 1);
};
