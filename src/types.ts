export type JwtTokens = {
  accessToken: string
  refreshToken: string
}

export type JwtValidatePayload = {
  sub: string
  email: string
}
