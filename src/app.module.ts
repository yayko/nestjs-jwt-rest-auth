import { AuthModule } from '@/modules/auth/auth.module'
import { MailModule } from '@/modules/mail/mail.module'
import { UserModule } from '@/modules/user/user.module'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { TypeOrmModule } from '@nestjs/typeorm'
import { SnakeNamingStrategy } from 'typeorm-naming-strategies'
import * as process from 'process'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: [`.env.${process.env.NODE_ENV}`, '.env']
    }),
    TypeOrmModule.forRootAsync({
      useFactory: () => {
        return {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          type: process.env.DB_TYPE as any,
          host: process.env.DB_HOST,
          port: process.env.DB_PORT as unknown as number,
          username: process.env.DB_USERNAME,
          password: process.env.DB_PASSWORD,
          database: process.env.DB_NAME,
          entities: ['dist/**/*.entity{.ts,.js}'],
          migrations: ['src/migrations/*{.ts}'],
          synchronize: process.env.NODE_ENV === 'development',
          namingStrategy: new SnakeNamingStrategy()
        }
      }
    }),
    AuthModule,
    UserModule,
    MailModule
  ]
})
export class AppModule {}
