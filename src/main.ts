import { BadRequestException,  ValidationError, ValidationPipe } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { NestExpressApplication } from '@nestjs/platform-express'
import { AppModule } from './app.module'
import * as cookieParser from 'cookie-parser'
import { registerHbsHelpers } from '@/common/lib/HbsHelpers'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'

require('module-alias/register')

registerHbsHelpers()

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    cors: {
      credentials: true,
      origin: [process.env.APP_DOMAIN, 'http://localhost:3000']
    }
  })

  // Start:: Cookie Parser
  app.use(cookieParser())
  // END:: Cookie Parser

  // Start:: Swagger configuration
  const swaggerConfig = new DocumentBuilder()
    .setTitle('NestJS API Rest Auth')
    .setDescription('NestJS API description')
    .setVersion('1.0')
    .build()

  const documentFactory = SwaggerModule.createDocument(app, swaggerConfig)
  SwaggerModule.setup('api', app, documentFactory)

  // Start:: Global Pipes
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions: { enableImplicitConversion: true },
      whitelist: true,
      exceptionFactory: (errors: ValidationError[]) => {
        const parsedError = {
          statusCode: '400',
          messages: {},
          error: 'Bad Request'
        }

        errors.forEach(error => {
          parsedError.messages[error.property] = Object.values(error.constraints).join('. ').trim()
        })
        return new BadRequestException(parsedError)
      }
    })
  )
  // END:: Global Pipes

  await app.listen(3001)
}

bootstrap()
