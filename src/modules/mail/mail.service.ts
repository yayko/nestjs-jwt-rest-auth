import { MailerService } from '@nestjs-modules/mailer';
import { Injectable, Logger } from '@nestjs/common';
import { User } from '@/common/entities';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class MailService {
  constructor(
    private mailerService: MailerService,
    private configService: ConfigService,
  ) {}

  async signupConfirmation(user: User) {
    const { firstName, verificationToken } = user;
    const url = `${process.env.APP_DOMAIN}/account/confirm?verificationToken=${verificationToken}`;

    if (this.configService.get('ENABLE_SIGN_UP_CONFIRM') === '1') {
      try {
        await this.mailerService.sendMail({
          to: user.email,
          subject: 'Welcome to our Application!',
          template: './signup-confirm',
          context: {
            name: firstName,
            url,
          },
        });
        Logger.log(`--- Confirmation email send to: ${user.email}`);
      } catch (e) {
        Logger.error('*** Could not send confirmation email', e);
      }
    }
  }

  async requestPasswordReset(email, token) {
    try {
      await this.mailerService.sendMail({
        to: email,
        subject: 'Password reset',
        template: './reset-password',
        context: {
          email,
          link: `${process.env.APP_DOMAIN}/auth/reset-password/${token}`,
        },
      });
      Logger.log(`--- [MAILER][PASSWORD_RESET] Sent to: ${email}`);
    } catch (e) {
      Logger.error(
        `--- [MAILER][PASSWORD_RESET][ERROR] Mail send error: ${email}\n\n${JSON.stringify(
          e,
        )}`,
      );
    }
  }
}
