import { UserService } from '@/modules/user/user.service';
import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  Logger,
  UnprocessableEntityException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcryptjs from 'bcryptjs';

import { CreateUserDto } from '@/modules/auth/dto/create-user.dto';
import { MailService } from '@/modules/mail/mail.service';
import { ResetPasswordDto } from '@/modules/auth/dto/reset-password.dto';
import { JwtTokens } from '@/types';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '@/common/entities';
import { Repository } from 'typeorm';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private jwtService: JwtService,
    private userService: UserService,
    private mailService: MailService,
    private configService: ConfigService,
  ) {}

  async validateUser(
    email: string,
    pass: string,
  ): Promise<Express.User | null> {
    const user = await this.userService.findOneByEmail(email);

    if (user) {
      const passwordMatches = await bcryptjs.compare(pass, user.passwordHash);
      if (passwordMatches)
        return {
          id: user.id,
          email: user.email,
          firstName: user.firstName,
          rbacRoles: user.userRoles?.map((userRole) => userRole.roleName) || [],
        };
    }
    return null;
  }

  async signIn(user: Express.User): Promise<JwtTokens> {
    return await this.getRegeneratedTokens(user);
  }

  async signOut(refreshToken: string) {
    const { sub: userId } = this.jwtService.decode(refreshToken);
    try {
      await this.userRepository.update(userId, {
        refreshTokenHash: null,
      });
    } catch (e) {
      Logger.log('Could not update data on: LOGOUT', e);
    }
  }

  async signUp(data: CreateUserDto): Promise<JwtTokens | false> {
    const user = await this.userService.createUser(data);
    if (!user) throw new UnprocessableEntityException('Could not create user');

    await this.mailService.signupConfirmation(user);
    return await this.getRegeneratedTokens({
      id: user.id,
      email: user.email,
      firstName: user.firstName,
      rbacRoles: user.userRoles?.map((userRole) => userRole.roleName) || [],
    });
  }

  async confirmSignUp(verificationToken: string) {
    if (!verificationToken)
      throw new BadRequestException('Verification token not found');

    const user =
      await this.userService.findOneByVerificationToken(verificationToken);
    if (!user)
      throw new BadRequestException(
        'This account has already been activated or the verification token is invalid',
      );
    await this.userRepository.update(user.id, {
      verificationToken: null,
      verifiedAt: new Date(),
    });

    return {
      statusCode: 200,
      message: 'OK',
    };
  }

  async refreshToken(refreshToken: string) {
    const { sub: id } = this.jwtService.decode(refreshToken);

    const user = await this.userService.findOne({ id });

    if (!user || !user.refreshTokenHash)
      throw new ForbiddenException('Access denied');

    const refreshTokenMatches = await bcryptjs.compare(
      refreshToken,
      user.refreshTokenHash,
    );

    if (!refreshTokenMatches) throw new BadRequestException('Invalid token');
    return await this.getRegeneratedTokens({
      id: user.id,
      email: user.email,
      firstName: user.firstName,
      rbacRoles: user.userRoles.map((userRole) => userRole.roleName),
    });
  }

  /**
   * Generates a new pair of accessToken and refreshToken
   */
  async getTokens(user: Express.User): Promise<JwtTokens> {
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.sign(
        {
          sub: user.id,
          ...user,
        },
        {
          secret: this.configService.get('JWT_ACCESS_TOKEN_SECRET'),
          expiresIn: this.configService.get('JWT_ACCESS_TOKEN_EXPIRATION_TIME'),
        },
      ),
      this.jwtService.sign(
        {
          sub: user.id,
          email: user.email,
        },
        {
          expiresIn: this.configService.get(
            'JWT_REFRESH_TOKEN_EXPIRATION_TIME',
          ),
          secret: this.configService.get('JWT_REFRESH_TOKEN_SECRET'),
        },
      ),
    ]);

    return {
      accessToken,
      refreshToken,
    };
  }

  /**
   * Updates user's refreshTokenHash in database
   * and revokes latest refreshToken
   */
  async updateRefreshTokenHash(userId: string, refreshToken: string) {
    const refreshTokenHash = await bcryptjs.hash(refreshToken, 10);

    // delete existing user's refresh tokens
    await this.userRepository.update(
      { id: userId },
      {
        refreshTokenHash,
      },
    );
  }

  /**
   * Generates a new pair of accessToken and refreshToken tokens,
   * updates user refreshTokenHash
   * and return newly generated tokens
   */
  async getRegeneratedTokens(user: Express.User): Promise<JwtTokens> {
    const tokens = await this.getTokens(user);
    await this.updateRefreshTokenHash(user.id, tokens.refreshToken);
    return tokens;
  }

  async requestPasswordReset(email: string) {
    try {
      const token = this.jwtService.sign(
        {
          sub: email,
        },
        {
          secret: this.configService.get('JWT_PASSWORD_RESET_TOKEN_SECRET'),
          expiresIn: this.configService.get(
            'JWT_PASSWORD_RESET_TOKEN_EXPIRATION_TIME',
          ),
        },
      );
      await this.userRepository.update(
        { email },
        {
          passwordResetToken: token,
        },
      );

      await this.mailService.requestPasswordReset(email, token);

      Logger.log(`Request password reset: SUCCESS for: ${email}`);
    } catch (e) {
      Logger.log(`Request password reset: ERROR for: ${email}`);
    }
  }

  async verifyPasswordResetToken(token: string) {
    try {
      this.jwtService.verify(token, {
        secret: this.configService.get('JWT_PASSWORD_RESET_TOKEN_SECRET'),
      });
    } catch (error) {
      return false;
    }
    return true;
  }

  async resetPassword({ token, new_password }: ResetPasswordDto) {
    const { sub } = this.jwtService.decode(token);

    const user = await this.userRepository.findOneBy({
      email: sub,
    });

    if (!user || !user.passwordResetToken)
      throw new BadRequestException('Invalid token');

    const refreshTokenMatches = token === user.passwordResetToken;
    const jwtValid = this.verifyPasswordResetToken(token);

    if (!refreshTokenMatches || !jwtValid)
      throw new BadRequestException('Invalid token');

    const passwordHash = await bcryptjs.hash(new_password, 10);
    await this.userRepository.update(user.id, {
      passwordHash,
      passwordResetToken: null,
    });
  }
}
