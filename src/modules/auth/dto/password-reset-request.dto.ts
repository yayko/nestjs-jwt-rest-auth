import { IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger'

export class PasswordResetRequestDto {
  @ApiProperty()
  @IsEmail()
  @IsString()
  public email: string;
}
