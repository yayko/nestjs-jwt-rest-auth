import {
  IsEmail,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger'

export class CreateUserDto {
  @ApiProperty()
  @IsEmail(
    {},
    {
      message: 'Adres email jest nieprawidłowy',
    },
  )
  @IsString()
  public email: string;

  @ApiProperty()
  @IsString()
  @MinLength(8, { message: 'Hasło jest zbyt krótkie' })
  @MaxLength(80, { message: 'Hasło jest za długie' })
  @Matches(/(?:(?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message:
      'Hasło powinno zawierać conajmniej 8 znaków, jedną dużą literę, znak specjalny, bez znaków diakrytycznych',
  })
  public password: string;
}
