import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger'

export class ResetPasswordDto {
  @ApiProperty()
  @IsString()
  public token: string;

  @ApiProperty()
  @IsString()
  public new_password: string;
}
