import { ApiProperty } from '@nestjs/swagger'
import { IsString } from 'class-validator'

export class ConfirmSignupDto {
  @ApiProperty()
  @IsString()
  public verificationToken: string;
}
