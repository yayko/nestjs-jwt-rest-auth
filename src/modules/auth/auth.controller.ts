import { AuthService } from '@/modules/auth/auth.service'
import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Req,
  Res,
  UnauthorizedException,
  UseGuards
} from '@nestjs/common'
import { LocalAuthGuard } from '@/modules/auth/guards/local-auth.guard'
import { CreateUserDto } from '@/modules/auth/dto/create-user.dto'
import { Request, Response } from 'express'
import AuthCookie from '@/common/lib/AuthCookie'
import { PasswordResetRequestDto } from '@/modules/auth/dto/password-reset-request.dto'
import { ResetPasswordDto } from '@/modules/auth/dto/reset-password.dto'
import { ApiBody, ApiCreatedResponse, ApiOkResponse } from '@nestjs/swagger'
import { SignInDto } from '@/modules/auth/dto/sign-in.dto'
import { ConfirmSignupDto } from '@/modules/auth/dto/confirm-signup.dto'

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOkResponse()
  @ApiBody({
    type: SignInDto,
    examples: {
      req: {
        value: {
          email: 'user@example.com',
          password: '123qweASD'
        }
      }
    }
  })
  @UseGuards(LocalAuthGuard)
  @Post('sign-in')
  async signIn(@Req() req: Request, @Res() res: Response) {
    const result = await this.authService.signIn(req.user)
    AuthCookie.setHeader(res, {
      accessToken: result.accessToken,
      refreshToken: result.refreshToken
    })
    res.status(HttpStatus.OK).send(result)
    return result
  }

  @ApiOkResponse()
  @Post('sign-out')
  async signOut(@Req() req: Request, @Res() res: Response) {
    const refreshToken = req.cookies.refreshToken
    if (!refreshToken) res.status(HttpStatus.OK).send()

    await this.authService.signOut(refreshToken)
    AuthCookie.removeHeader(res)
    res.status(HttpStatus.OK).send()
  }

  @Post('sign-up')
  @ApiCreatedResponse()
  async signUp(@Body() body: CreateUserDto) {
    return await this.authService.signUp(body)
  }

  @Post('confirm')
  async signUpConfirm(@Body() body: ConfirmSignupDto) {
    return await this.authService.confirmSignUp(body.verificationToken)
  }

  @Post('refresh-token')
  async refreshToken(@Req() req: Request, @Res() res: Response) {
    const refreshToken = req.cookies.refreshToken
    if (!refreshToken || refreshToken === 'undefined') {
      AuthCookie.removeHeader(res)
      throw new UnauthorizedException()
    }
    const newTokens = await this.authService.refreshToken(refreshToken)

    AuthCookie.setHeader(res, newTokens)

    res.status(HttpStatus.OK).send(newTokens)
  }

  @Post('request-password-reset')
  async requestPasswordReset(@Body() body: PasswordResetRequestDto) {
    await this.authService.requestPasswordReset(body.email)
  }

  @Get('verify-password-reset-token/:token')
  async verifyPasswordResetToken(@Param('token') token: string) {
    if (!token) throw new BadRequestException('Invalid token')
    return await this.authService.verifyPasswordResetToken(token)
  }

  @Post('reset-password')
  async resetPassword(@Body() body: ResetPasswordDto) {
    await this.authService.resetPassword(body)
  }
}
