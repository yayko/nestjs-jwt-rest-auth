import { HttpException, HttpStatus, Injectable, UnprocessableEntityException } from '@nestjs/common'
import * as bcryptjs from 'bcryptjs'
import { CreateUserDto } from '@/modules/auth/dto/create-user.dto'
import { randomString } from '@/common/utils/generateRandomString'
import { ConfigService } from '@nestjs/config'
import { FindOneOptions, IsNull, Not, Repository } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'
import { User, UserRole } from '@/common/entities'
import { UserStatus } from '@/common/constants/UserStatus'
import { RbacRole } from '@/common/constants/RbacRole'

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private configService: ConfigService
  ) {}

  async findOne(where: FindOneOptions<User>['where']): Promise<User> {
    const user = await this.userRepository.findOne({
      where: {
        ...where,
        verifiedAt: Not(IsNull())
      },
      relations: ['userRoles']
    })
    if (user) return user
    throw new HttpException('Not found', HttpStatus.NOT_FOUND)
  }

  async findOneByEmail(email: string): Promise<User | undefined> {
    return this.userRepository.findOneBy({
      email,
      verifiedAt: Not(IsNull())
    })
  }

  async findOneByVerificationToken(verificationToken: string): Promise<User | undefined> {
    return this.userRepository.findOneBy({ verificationToken })
  }

  async createUser(data: CreateUserDto): Promise<User> {
    const verificationEnabled = this.configService.get('ENABLE_SIGN_UP_CONFIRM') === '1'
    const { password, email } = data
    const passwordHash = await bcryptjs.hash(password, 10)
    const userExists = await this.userRepository.findOneBy({
      email
    })

    if (userExists) {
      throw new UnprocessableEntityException('Account with this email already exists')
    }

    return this.userRepository.manager.transaction(async manager => {
      const user = await manager.save(User, {
        email,
        passwordHash,
        status: UserStatus.ACTIVE,
        verificationToken: verificationEnabled ? randomString() : null,
        verifiedAt: verificationEnabled ? null : new Date()
      })

      await manager.save(UserRole, {
        userId: user.id,
        roleName: RbacRole.USER
      })
      return user
    })
  }
}
