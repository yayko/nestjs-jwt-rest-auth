import { JwtAuthGuard } from '@/modules/auth/guards/jwt-auth.guard'
import { UserService } from '@/modules/user/user.service'
import { Controller, Get, Logger, Param, UseGuards } from '@nestjs/common'
import { CurrentUser } from '@/common/decorators'

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('me')
  @UseGuards(JwtAuthGuard)
  async me(@CurrentUser() user: Express.User) {
    Logger.log('User', user)
    return user
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.userService.findOne({ id })
  }
}
