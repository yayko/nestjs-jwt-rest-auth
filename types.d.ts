declare namespace Express {
}

export {};

declare global {
  namespace Express {
    // Update Express.User to include user's data
    export interface User extends AppUser {
      readonly id: string | null;
      readonly email: string | null;
      readonly firstName: string | null;
      readonly rbacRoles: string[];
    }
  }
}
